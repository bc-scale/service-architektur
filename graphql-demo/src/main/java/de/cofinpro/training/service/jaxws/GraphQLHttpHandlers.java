package de.cofinpro.training.service.jaxws;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.util.Headers;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class GraphQLHttpHandlers {

    private static final Jsonb JSONB = JsonbBuilder.create();
    private GraphQLSchema schema;

    GraphQLHttpHandlers(GraphQLSchema schema) {
        this.schema = schema;
    }

    HttpHandler graphqlPostRequest() {
        return new BlockingHandler(exchange -> {
            final String query = toString(exchange.getInputStream());
            final ExecutionInput executionInput = ExecutionInput.newExecutionInput(query).build();
            executeQuery(exchange, executionInput);
        });
    }

    HttpHandler graphqlJSONRequest() {
        return new BlockingHandler(exchange -> {
            final GraphQLRequest request = JSONB.fromJson(exchange.getInputStream(), GraphQLRequest.class);
            final ExecutionInput input = ExecutionInput.newExecutionInput()
                    .query(request.getQuery())
                    .operationName(request.getOperation())
                    .variables(request.getVariables())
                    .build();
            executeQuery(exchange, input);
        });
    }

    HttpHandler graphQLGetRequest() {
        return new BlockingHandler(exchange -> {
            final Map<String, Deque<String>> queryParams = exchange.getQueryParameters();
            final ExecutionInput.Builder input = ExecutionInput.newExecutionInput((queryParams.get("query").getFirst()));

            final Deque<String> operation = queryParams.get("operation");
            if (operation != null) {
                input.operationName(operation.getFirst());
            }

            final Deque<String> variables = queryParams.get("variables");
            if (variables != null) {
                final Type variablesType = new HashMap<String, Object>(){}.getClass().getGenericSuperclass();
                input.variables(JSONB.fromJson(variables.getFirst(), variablesType));
            }

            executeQuery(exchange, input.build());
        });
    }

    private void executeQuery(HttpServerExchange exchange, ExecutionInput input) {
        final GraphQL graphQL = GraphQL.newGraphQL(schema).build();
        final ExecutionResult result = graphQL.execute(input);
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
        JSONB.toJson(result.toSpecification(), exchange.getOutputStream());
    }

    private static String toString(InputStream is) throws IOException {
        try (var reader = new InputStreamReader(is, StandardCharsets.UTF_8);
             var br = new BufferedReader(reader)) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }



}
