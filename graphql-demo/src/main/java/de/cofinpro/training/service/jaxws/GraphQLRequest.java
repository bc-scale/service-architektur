package de.cofinpro.training.service.jaxws;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO for GraphQL Queries as defined by the Spec
 *
 * @author Gregor Tudan, Cofinpro AG
 */
public class GraphQLRequest {
    private String query;
    private String operation;
    private Map<String, Object> variables = new HashMap<>();

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }
}
