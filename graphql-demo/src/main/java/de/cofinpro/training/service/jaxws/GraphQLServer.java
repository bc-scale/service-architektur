package de.cofinpro.training.service.jaxws;

import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.predicate.Predicate;
import io.undertow.predicate.Predicates;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.*;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;

import java.io.InputStream;
import java.io.InputStreamReader;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;
import static io.undertow.Handlers.*;
import static io.undertow.attribute.ExchangeAttributes.*;
import static io.undertow.util.Headers.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class GraphQLServer {
    final Undertow undertow;

    private GraphQLServer() {
        final TypeDefinitionRegistry registry = new SchemaParser().parse(new InputStreamReader(readSchemaFile()));
        final RuntimeWiring wiring = GraphQLDataFetchers.buildRuntimeWiring();
        final GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(registry, wiring);
        final GraphQLHttpHandlers graphqlHandlers = new GraphQLHttpHandlers(schema);

        this.undertow = Undertow.builder()
                .addHttpListener(8080, "localhost")
                .setHandler(
                        new RoutingHandler()
                                .get("/graphql", graphqlHandlers.graphQLGetRequest())
                                .post("/graphql", contentType("application/json"), graphqlHandlers.graphqlJSONRequest())
                                .post("/graphql", contentType("application/graphql"), graphqlHandlers.graphqlPostRequest())
                        .setFallbackHandler(staticHandler())
                ).build();
    }

    private static ResourceHandler staticHandler() {
        return new ResourceHandler(
                new ClassPathResourceManager(GraphQLServer.class.getClassLoader(), "static"))
                .setWelcomeFiles("index.html");
    }

    private static Predicate contentType(String contentType) {
        return Predicates.contains(requestHeader(CONTENT_TYPE), contentType);
    }

    public static void main(String[] args) {
        new GraphQLServer().start();
    }

    private void start() {
        undertow.start();
    }


    private static InputStream readSchemaFile() {
        var file = GraphQLServer.class.getClassLoader().getResourceAsStream("schema.graphql");
        assert file != null;
        return file;
    }
}
