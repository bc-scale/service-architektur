# GRPC Example

Ein Beispiel für gRPC mit und ohne Streams.

## Learn

* https://grpc.io
* https://developers.google.com/protocol-buffers/

## Run

Die Main-Methode von `de.cofinpro.training.service.grpc.server.Server` ausführen. 
Der Server hört dann auf Port 8080

Dann die Main-Methode von `de.cofinpro.training.service.grpc.client.BabelClient` ausführen.

