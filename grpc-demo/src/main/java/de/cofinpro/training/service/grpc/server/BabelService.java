package de.cofinpro.training.service.grpc.server;

import de.cofinpro.training.service.grpc.BabelGrpc;
import de.cofinpro.training.service.grpc.Text;
import io.grpc.stub.StreamObserver;

import java.util.List;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class BabelService extends BabelGrpc.BabelImplBase {
    private final Dictionary dictionary = new Dictionary();

    @Override
    public void translate(Text request, StreamObserver<Text> responseObserver) {
        final List<String> translations = dictionary.lookup(request.getText());
        for (String translation : translations) {
            responseObserver.onNext(Text.newBuilder().setText(translation).build());
        }
        responseObserver.onCompleted();
    }
}
