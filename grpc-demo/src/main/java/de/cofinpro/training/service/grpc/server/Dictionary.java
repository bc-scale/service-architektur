package de.cofinpro.training.service.grpc.server;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class Dictionary {

    private byte[] dict;

    Dictionary() {
        try (var stream = getClass().getClassLoader().getResourceAsStream("de-en.txt")) {
            if (stream == null) {
                throw new FileNotFoundException("Could not find dictionary file de-en.txt");
            }
            this.dict = stream.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all entries as stream
     * <p>
     * first item in the array will be the german phrase, the second the english translation.
     * <p>
     * Make sure to close the stream after use!
     *
     * @return a lazy stream of all dict entries
     */
    Stream<String[]> entryStream() throws IOException {
        var in = new ByteArrayInputStream(dict);
        var buffer = new BufferedReader(new InputStreamReader(in));

        while (true) {
            // skip the header
            if (!buffer.readLine().startsWith("#")) break;
        }

        return buffer.lines().map(line -> line.split("::"));
    }

    List<String> lookup(String text) {
        try (var in = new ByteArrayInputStream(dict);
             var reader = new InputStreamReader(in);
             var buffer = new BufferedReader(reader)) {

            while (true) {
                // skip the header
                if (!buffer.readLine().startsWith("#")) break;
            }

            final var firstWordPattern = Pattern.compile("([\\w\\s]+).*");
            final Matcher matcher = firstWordPattern.matcher("");
            String line;

            while ((line = buffer.readLine()) != null) {
                if (isAMatch(text, line, matcher)) {
                    final String[] entry = line.split("::");
                    return Arrays.asList(entry[1].trim().split(";\\s?"));
                }
                // Not there yet - keep looping
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    private boolean isAMatch(String text, String entry, Matcher matcher) {
        matcher.reset(entry);
        if (!matcher.matches()) {
            // cannot parse
            return false;
        }
        final String firstWord = matcher.group(1);
        return firstWord.equalsIgnoreCase(text);
    }
}
