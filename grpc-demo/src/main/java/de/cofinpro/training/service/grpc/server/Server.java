package de.cofinpro.training.service.grpc.server;

import io.grpc.ServerBuilder;

import java.io.IOException;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class Server {

    private final io.grpc.Server server;

    public Server() {
        this.server = ServerBuilder.forPort(8080).addService(new BabelService()).build();
    }

    void start() throws IOException, InterruptedException {
        server.start();
        server.awaitTermination();
    }

    public static void main(String[] args) throws Exception {
        new Server().start();
    }
}
