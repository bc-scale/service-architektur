package de.cofinpro.training.service.grpc.client;

import de.cofinpro.training.service.grpc.BabelGrpc;
import de.cofinpro.training.service.grpc.server.BabelService;
import io.grpc.ManagedChannel;
import io.grpc.Server;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.*;
import static org.mockito.AdditionalAnswers.delegatesTo;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class BabelClientTest {

    private static BabelClient client;
    private static Server server;

    @BeforeAll
    static void setUp() throws Exception {
        final String serverName = InProcessServerBuilder.generateName();

        // You might want to mock Babel
        final BabelGrpc.BabelImplBase serviceImpl = mock(BabelGrpc.BabelImplBase.class, delegatesTo(new BabelService()));

        server = InProcessServerBuilder.forName(serverName).directExecutor().addService(serviceImpl).build().start();
        ManagedChannel channel = InProcessChannelBuilder.forName(serverName).directExecutor().build();
        client = new BabelClient(channel);
    }

    @AfterAll
    static void tearDown() {
        client.close();
        server.shutdown();
    }

    @Test
    void testBlockingTranslate() {
        final List<String> translation = client.translate("Hallo");
        assertFalse(translation.isEmpty());
        assertEquals("Hi!", translation.get(0));
    }

    @Test
    void testAsyncTranslate() {
        final Consumer<String> consumer = mock(Consumer.class);
        client.translate("Hallo", consumer);
        verify(consumer, times(2)).accept(anyString());
        verify(consumer).accept("Hi!");
    }

}
