package de.cofinpro.training.service.grpc.server;

import de.cofinpro.training.service.grpc.BabelGrpc;
import de.cofinpro.training.service.grpc.Text;
import io.grpc.ManagedChannel;
import io.grpc.Server;
import io.grpc.inprocess.InProcessChannelBuilder;
import io.grpc.inprocess.InProcessServerBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class ServerTest {

    private Server server;
    private ManagedChannel channel;

    @BeforeEach
    void setUp() throws IOException {
        String serverName = InProcessServerBuilder.generateName();
        server = InProcessServerBuilder.forName(serverName).directExecutor().addService(new BabelService()).build().start();
        channel = InProcessChannelBuilder.forName(serverName).directExecutor().build();
    }

    @AfterEach
    void tearDown() {
        channel.shutdown();
        server.shutdown();
    }

    @Test
    void testServer() {
        BabelGrpc.BabelBlockingStub blockingStub = BabelGrpc.newBlockingStub(channel);

        Iterator<Text> reply = blockingStub.translate(Text.newBuilder().setText("Hallo").build());

        assertEquals("Hi!", reply.next().getText());
    }

}
