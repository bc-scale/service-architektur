package de.cofinpro.training.service.jaxws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
@WebService(name = "Babel")
interface Babel {

    @WebMethod
    @WebResult(name = "translation")
    String translate(
            @WebParam(name = "locale") String locale,
            @WebParam(name = "word") String text);
}
