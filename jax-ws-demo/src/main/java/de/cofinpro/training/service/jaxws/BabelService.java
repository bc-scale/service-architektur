package de.cofinpro.training.service.jaxws;

import javax.jws.WebService;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.soap.Addressing;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
@WebService(serviceName = "BabelService", portName = "babel")
@Addressing()
public class BabelService implements Babel {

    private static Dictionary dictionary = new Dictionary();

    @Override
    public String translate(String locale, String text) {
       if (!locale.equals("de_en")) {
           throw new UnsupportedOperationException("Sorry, we can only do german to english (de_en)");
        }

       return dictionary.lookup(text).orElse(null);
    }
}
