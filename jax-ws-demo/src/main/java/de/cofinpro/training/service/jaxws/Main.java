package de.cofinpro.training.service.jaxws;

import javax.xml.ws.Endpoint;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class Main {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/", new BabelService());
    }
}
